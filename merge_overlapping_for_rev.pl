#  Example of the file that we are going to process
#
# 10:76756813-76756948	10	76756813	76756948	-	136	0	0	1	0	0
# 10:76822856-76822998	10	76822856	76822998	+	143	6	2	9	7	0
# 10:76823897-76824039	10	76823897	76824039	+	143	0	1	1	2	1
# 10:76824415-76824557	10	76824415	76824557	-	143	4	0	2	30	0
# 10:76831824-76831966	10	76831824	76831966	+	143	5	1	1	4	0
# 10:76845545-76845687	10	76845545	76845687	-	143	2	1	2	2	0
# 10:76845684-76845826	10	76845684	76845826	+	143	1	1	0	0	0
# 10:76848069-76848211	10	76848069	76848211	-	143	3	0	0	3	0
# 10:76850237-76850379	10	76850237	76850379	+	143	0	0	1	1	0
# 10:76872713-76872779	10	76872713	76872779	-	67	0	0	0	1	0
# 10:76883803-76883933	10	76883803	76883933	-	131	1	0	1	0	0
use warnings;
$count = 0;
open( FILE, $ARGV[0] ) || die;    # open the file

while ( $line = <FILE> ) {        # read the file line by line
    if ( $count eq "0" ) {        # If we are at the first line
        $count++;
        $previous_line = $line;    # Save it for the second round
    }
    else {
        @current  = split( /\t/, $line );
        @previous = split( /\t/, $previous_line );

        # calculate the difference of the coordinates
        if ( $current[1] eq $previous[1] ) {    # check chromosome
            $difference = $previous[3] - $current[2];
        }
        else {
            $difference = 0 - $current[2];
        }
        if ( $difference > 0 )
        {    # means if $difference is positive, there is overlapping
                # print the coordinates
            print
"$current[1]:$current[2]-$current[3]\t$current[1]\t$current[2]\t$current[3]\t+\t$difference\t";

            # and iterate for each sample
            for ( $i = 6 ; $i <= $#current ; $i++ ) {
                my $sum = $previous[$i] + $current[$i];
                print "$sum\t";
            }

            # then print the carriage return to end the line
            print "\n";
            $count = 0;

            #next LINE;
        }
        else {
            print "$previous_line";
        }

        # and save the current line as the previous
        $previous_line = $line;
    }    # end if ($. eq 0)
}                          # end while (<>)
print "$previous_line";    # and print the last line
