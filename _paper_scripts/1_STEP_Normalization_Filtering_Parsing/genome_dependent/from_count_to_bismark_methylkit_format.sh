# Genome Dependent #
# "IMPORTANT" before running commands check samples order
# WARNING: this is an example taken from our paper


# creates the directory where results are stored
mkdir -p rpm

# normalization dataset (indicate fields where the samples are)
perl normalization.pl --file_input counts_genome_dependent.txt --field 7-8-9-10-11-12 --file_output normalized_data.txt

# remove any rows with 0 0 0 0 0 0 after normalization
awk '{ sum=$7+$8+$9+$10+$11+$12; if (sum >0) print $0 }' normalized_data.txt > normalized_data_no_0.txt

# filtering dataset
# --field: columns where samples rely: use  "£" to distinguish treatments,  "-" for replicas
# --average_threshold: keep samples where average is over the threshold
# --SD_threshold: keep liens where SD is lower (valid even if only one treatment exceed)
# --treat_threshold: comparison threshold between the two treats
perl filtering.pl --file_input normalized_data_no_0.txt --field 7-8-9£10-11-12  --average_threshold 4  --SD_threshold  10 --treat_threshold 0 > normalized_filtered_data.txt


# creates an intermediate bismark form file with methyl-sensitive restriction enzyme site
awk '{ if ($5 == "+") print $2 "\t" $3 "\t" $5 "\t" $7 "\t" $8 "\t" $9 "\t" $10 "\t" $11 "\t" $12; if ($5 == "-") print $2 "\t" $4 "\t" $5 "\t" $7 "\t" $8 "\t" $9 "\t" $10 "\t" $11 "\t" $12; }' normalized_filtered_data.txt > normalized_filtered_data_MSE_detail.txt


# estimation of methylated loci
# --file input_file.txt
# --field selected data columns
# --param use max value as reference for calculate methylation at each locus
perl estimation_meth_loci.pl --file normalized_filtered_data_MSE_detail.txt --field 4-5-6-7-8-9 --param max > partial_bismark_data.txt


# creates bismark format
awk '{print $1"\t"$2"\t"$3"\t"$11"\t"$4"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_wt1.txt
awk '{print $1"\t"$2"\t"$3"\t"$12"\t"$5"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_wt2.txt
awk '{print $1"\t"$2"\t"$3"\t"$13"\t"$6"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_wt3.txt
awk '{print $1"\t"$2"\t"$3"\t"$14"\t"$7"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_stress1.txt
awk '{print $1"\t"$2"\t"$3"\t"$15"\t"$8"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_stress2.txt
awk '{print $1"\t"$2"\t"$3"\t"$16"\t"$9"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_stress3.txt

# makes either calculations or parsing data in order to prepare methylkit format dataset
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_wt1.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_wt1.txt
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_wt2.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_wt2.txt
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_wt3.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_wt3.txt
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_stress1.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_stress1.txt
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_stress2.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_stress2.txt
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_stress3.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_stress3.txt
