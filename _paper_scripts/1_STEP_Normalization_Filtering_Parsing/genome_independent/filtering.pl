#!/usr/bin/perl
use strict;
use Getopt::Long;

my $file_input;
my $file_output;
my $stringa;
my $threshold1;
my $threshold2;
my $threshold3;

GetOptions(

    "file_input=s"        => \$file_input,
    "field=s"             => \$stringa,
    "file_output=s"       => \$file_output,
    "average_threshold=i" => \$threshold1,
    "SD_threshold=i"      => \$threshold2,
    "treat_threshold=i"   => \$threshold3,

  )
  or die(
    print
      "arguments: file_input field file_output average_threshold SD_threshold\n"
  );

my @tmp = split( "£", $stringa );

my @AoA = &load_file_as_AoA($file_input);

my @AoA_field;
for ( my $i = 0 ; $i < scalar @tmp ; $i++ ) {
    my @array = split( "-", $tmp[$i] );
    push( @AoA_field, [@array] );
}

for ( my $i = 0 ; $i < scalar @AoA ; $i++ ) {
    my @means = ();
    my @std   = ();
    for ( my $j = 0 ; $j < scalar @AoA_field ; $j++ ) {
        my @calcoli = ();
        for ( my $k = 0 ; $k < scalar @{ $AoA_field[$j] } ; $k++ ) {
            push( @calcoli, $AoA[$i][ $AoA_field[$j][$k] - 1 ] );
        }
        my $mean = &calculate_media( \@calcoli );
        my $std = &calculate_std_dev( $mean, \@calcoli );

        push( @means, $mean );
        push( @std,   $std );
    }
    my $mean_treatments = &calculate_media( \@means );
    my $std_treatments  = &calculate_std_dev( $mean_treatments, \@means );
    my $condizione1     = &calculate_condizione1( \@means, $threshold1 );
    my $condizione2     = &calculate_condizione2( \@std, $threshold2 );

    if (   ( $condizione1 > 0 )
        && ( $condizione2 > 0 )
        && ( $std_treatments > $threshold3 ) )
    {
        print( join( "\t", @{ $AoA[$i] } ), "\n" );
    }
}

sub calculate_media {
    my @self  = @_;
    my @array = @{ $self[0] };
    my $tot;
    for ( my $i = 0 ; $i < scalar @array ; $i++ ) {
        $tot = $tot + $array[$i];
    }
    my $mean = $tot / scalar @array;
    return $mean;
}

sub calculate_std_dev {
    my @self    = @_;
    my $average = $self[0];
    my @values  = @{ $self[1] };

    my $count       = scalar @values;
    my $std_dev_sum = 0;
    $std_dev_sum += ( $_ - $average )**2 for @values;

    return $count ? sqrt( $std_dev_sum / $count ) : 0;
}

sub calculate_condizione1 {
    my @self        = @_;
    my @array       = @{ $self[0] };
    my $threshold   = $self[1];
    my $condizione1 = 1;
    for ( my $i = 0 ; $i < scalar @array ; $i++ ) {
        my $count = 0;
        $count = 1 if $array[$i] >= $threshold;
        $condizione1 = $condizione1 * $count;

    }
    return $condizione1;
}

sub calculate_condizione2 {
    my @self        = @_;
    my @array       = @{ $self[0] };
    my $threshold   = $self[1];
    my $condizione2 = 1;
    for ( my $i = 0 ; $i < scalar @array ; $i++ ) {
        my $count = 0;
        $count = 1 if $array[$i] <= $threshold;
        $condizione2 = $condizione2 * $count;

    }
    return $condizione2;
}

sub load_file_as_AoA {
    my @self      = @_;
    my $file_name = $self[0];
    my $separator = $self[1] || "\t";
    my @array     = &load_file_as_array("$file_name");
    my @AoA;

    for ( my $j = 0 ; $j < scalar @array ; $j++ ) {
        chomp $array[$j];
        my @tmp = split( "$separator", $array[$j] );
        push( @AoA, [@tmp] );
    }
    return @AoA;
}

sub load_file_as_array {
    my @self      = @_;
    my $name_file = $self[0];
    open( FILE, '<', "$name_file" );
    my @array = (<FILE>);
    close FILE;

    return @array;
}
