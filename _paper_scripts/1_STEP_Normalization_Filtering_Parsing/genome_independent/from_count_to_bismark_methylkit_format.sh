# Genome Independent #
# "IMPORTANT" before running commands check order samples
# WARNING: this is an example taken from our paper


#remove header from counts_genome_independent.txt
#ID	aci_Ctrl1.fq.gz.counts.trim.txt	aci_Ctrl2.fq.gz.counts.trim.txt	aci_Ctrl3.fq.gz.counts.trim.txt	aci_STR1.fq.gz.counts.trim.txaci_STR2.fq.gz.counts.trim.txt	aci_STR3.fq.gz.counts.trim.txt
#E1_L143	8	13	6	2	6	5
#E2_L143	24	35	25	8	17	11
#E3_L143	6	5	5	2	6	9

# Remove header
tail -n +2 counts_genome_independent.txt > counts_genome_independent_noheader.txt

#cg_counts_genome_independent.txt
#E1_L143	2	6	10	6
#E2_L143	5	17	29	24
#E3_L143	7	8	27	8

#Add row number
awk '{printf "%s,%s\n", NR, "\t"$0}' counts_genome_independent_noheader.txt | sed 's/,//' > counts_genome_independent_noheader_n.txt

#1	E1_L143	8	13	6	2	6	5
#2	E2_L143	24	35	25	8	17	11
#3	E3_L143	6	5	5	2	6	9

# Create directory where results are stored
mkdir -p rpm

# normalization dataset
perl normalization.pl --file_input counts_genome_independent_noheader_n.txt --field 3-4-5-6-7-8 --file_output normalized_data.txt

#1	E1_L143	3	4	2	1	3	4
#2	E2_L143	10	12	9	5	8	9
#3	E3_L143	3	2	2	1	3	7


# remove any rows with 0 0 0 0 0 0 after normalization
awk '{ sum=$3+$4+$5+$6+$7+$8; if (sum >0) print $0 }' normalized_data.txt > normalized_data_no_0.txt


# filtering dataset
# --field: columns where samples rely: use  "£" to distinguish treatments,  "-" for replicas
# --average_threshold: keep samples where average is over the threshold
# --SD_threshold: keep liens where SD is lower (valid even if only one treatment exceed)
# --treat_threshold: comparison threshold between the two treats
perl filtering.pl --file_input normalized_data_no_0.txt --field 3-4-5£6-7-8  --average_threshold 4  --SD_threshold  8 --treat_threshold 0 > normalized_filtered_data.txt

#2	E2_L143	10	12	9	5	8	9
#4	E4_L143	5	4	6	4	4	5
#7	E30_L143	6	5	5	3	10	6

# creates an intermediate bismark form file with methyl-sensitive restriction enzyme site
awk '{print "1""\t"$1"\t""+""\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8}' normalized_filtered_data.txt > normalized_filtered_data_MSE_detail.txt

#1	2	+	10	12	9	5	8	9
#1	4	+	5	4	6	4	4	5
#1	7	+	6	5	5	3	10	6


# estimation of methylated loci
# --file input_file.txt
# --field selected data columns
# --param use max value as reference for calculate methylation at each locus

perl estimation_meth_loci.pl --file normalized_filtered_data_MSE_detail.txt --field 4-5-6-7-8-9 --param max > partial_bismark_data.txt


# creates bismark format
awk '{print $1"\t"$2"\t"$3"\t"$11"\t"$4"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_wt1.txt
awk '{print $1"\t"$2"\t"$3"\t"$12"\t"$5"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_wt2.txt
awk '{print $1"\t"$2"\t"$3"\t"$13"\t"$6"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_wt3.txt
awk '{print $1"\t"$2"\t"$3"\t"$14"\t"$7"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_stress1.txt
awk '{print $1"\t"$2"\t"$3"\t"$15"\t"$8"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_stress2.txt
awk '{print $1"\t"$2"\t"$3"\t"$16"\t"$9"\t""CG""\t""CG"}' partial_bismark_data.txt > rpm/bismark_format_stress3.txt

# makes either calculations or parsing data in order to prepare methylkit format dataset
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_wt1.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_wt1_chr.txt
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_wt2.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_wt2_chr.txt
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_wt3.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_wt3_chr.txt
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_stress1.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_stress1_chr.txt
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_stress2.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_stress2_chr.txt
awk '{ sum=$4+$5; C=($4/sum)*100; T=($5/sum)*100 ; if ($6 == "CG" && sum >0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""%.2f""\t""%.2f""\n", sum,C,T; if ($6 == "CG" && sum==0) printf  $1"."$2"\t"$1"\t"$2"\t"$3"\t""%s""\t""0.00""\t""0.00""\n", sum,C,T }' rpm/bismark_format_stress3.txt | tr "[\+\-]" "[FR]" >  rpm/methylkit_format_stress3_chr.txt
