#!/usr/bin/perl
use strict;
use Getopt::Long;
use Math::BigFloat;

my ( $file_i, $stringa, $file_o );
GetOptions(
    "file_input=s"  => \$file_i,
    "field=s"       => \$stringa,
    "file_output=s" => \$file_o
  )
  or die(
    "Error in command line arguments,
	_ --file_input --field --file_output\n"
  );

my @AoA = &load_file_as_AoA("$file_i");

my @tmp = &extract_array_from_stringa($stringa);
my %hash;
my $tot;

for ( my $j = 0 ; $j < scalar @tmp ; $j++ ) {
    for ( my $i = 0 ; $i < scalar @AoA ; $i++ ) {

        $hash{ $tmp[$j] } = $hash{ $tmp[$j] } + $AoA[$i][ $tmp[$j] ];
        $tot = $tot + $AoA[$i][ $tmp[$j] ];
    }
}

for ( my $j = 0 ; $j < scalar @tmp ; $j++ ) {
    for ( my $i = 0 ; $i < scalar @AoA ; $i++ ) {
        my $FC      = 1000000 / ( $hash{ $tmp[$j] } );
        my $dato    = $AoA[$i][ $tmp[$j] ] * $FC;
        my $rounded = sprintf( "%.0f", $dato );
        $AoA[$i][ $tmp[$j] ] = $rounded;
    }
}

&print_array( \@AoA, "$file_o" );

########################################
sub extract_array_from_stringa {
    my @self    = @_;
    my $stringa = $self[0];
    my @field   = split( "-", "$stringa" );

    my @return = map { $_ - 1 } @field;
    return @return;
}

########################################
sub load_file_as_AoA {
    my @self       = @_;
    my $nome_file  = $self[0];
    my $col_inizio = $self[1] - 1;
    my $col_fine   = $self[2] - 1;

    my $separator = $self[3] || "\t";
    my @AoA;
    my @array = &load_file_as_array("$nome_file");
    for ( my $i = 0 ; $i < scalar @array ; $i++ ) {
        chomp $array[$i];
        my @tmp = split( "$separator", $array[$i] );
        push( @AoA, [@tmp] );
    }
    return @AoA;
}

########################################

sub load_file_as_array {
    my @self      = @_;
    my $name_file = $self[0];
    open( FILE, '<', "$name_file" );
    my @array = (<FILE>);
    close FILE;

    return @array;
}
########################################
sub print_array {
    my @self      = @_;
    my @AoA       = @{ $self[0] };
    my $nome_file = $self[1];
    my $mode_self = $self[2] || "OVERWRITE";

    my $mode = '>';
    $mode = '>>' if $mode_self eq "APPEND";

    open( OUTARRAY, "$mode", "$nome_file" ) if $mode_self =~ /APPEND|OVERWRITE/;
    &p_array( \@AoA );

    sub p_array {
        my $array_reference = shift;
        my $flag            = 0;

        foreach (@$array_reference) {
            if ( ref($_) eq 'ARRAY' ) {
                &p_array($_);
            }
            else {
                print OUTARRAY $_ . "\t";
                $flag = 1;
            }

        }
        print OUTARRAY "\n" if $flag == 1;
    }

    close OUTARRAY;
}
########################################
