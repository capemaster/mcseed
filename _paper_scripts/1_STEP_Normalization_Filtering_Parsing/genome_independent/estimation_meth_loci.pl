#!/usr/bin/perl
use strict;

use Getopt::Long;

my ( $file, $stringa, $param );
GetOptions(
    "file_name=s" => \$file,
    "field=s"     => \$stringa,
    "param=s"     => \$param

) or die("Error in command line arguments\n");

my @array_f = &load_file_as_array("$file");
for ( my $i = 0 ; $i < scalar @array_f ; $i++ ) {
    chomp $array_f[$i];
    my @tmp = &extract_array_from_stringa( $array_f[$i], $stringa );
    my @tmp_ordered = sort { $b <=> $a } @tmp if $param eq 'max';
    @tmp_ordered = sort { $a <=> $b } @tmp if $param eq 'min';
    my $print_s = &subtract_max( \@tmp, $tmp_ordered[0] );
    print( $array_f[$i], "\t", $tmp_ordered[0], "\t", $print_s, "\n" );
}

sub subtract_max {
    my @self       = @_;
    my @array      = @{ $self[0] };
    my $max        = $self[1];
    my @subtracted = map { $max - $_ } @array;
    my $print      = join( "\t", @subtracted );
    return $print;
}

sub extract_array_from_stringa {
    my @self    = @_;
    my $row     = $self[0];
    my $stringa = $self[1];
    my @field   = split( "-", "$stringa" );
    my @array   = split( "\t", "$row" );

    my @return;
    for ( my $i = 0 ; $i < scalar @field ; $i++ ) {
        my $index = $field[$i] - 1;
        push( @return, "$array[$index]" );
    }

    return @return;
}

sub load_file_as_array {
    my @self      = @_;
    my $name_file = $self[0];
    open( FILE, '<', "$name_file" );
    my @array = (<FILE>);
    close FILE;

    return @array;
}
