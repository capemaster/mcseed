# seq 100 100 2000 | parallel --keep 'perl make_clusters_windows6_tris.pl --file_names DMPs_meth_myDiff_BH.txt-2-2-1-0-0 --stringa_output DMPs_meth_myDiff_BH.txt-2  --max_dist {} --warn 1 --threshold 0.05 --prob 0.1 --mode strict > cluster2_coordinates_d{}_w1_t0.05_p0.1.txt'

# we choose windows of 100 bp from 100 to 2000 bp
for i in `seq 100 100 2000`;
do
  echo "Clustering with $i bp"
  perl make_clusters_windows6_tris.pl --file_names DMPs_meth_myDiff_BH.txt-2-2-1-0-0 --stringa_output DMPs_meth_myDiff_BH.txt-2  --max_dist $i --warn 1 --threshold 0.05 --prob 0.1 --mode strict > cluster_"$i".txt
  # remove unwanted output
  rm bingo*
  # calculate useful info for window choice
  printf "cluster_$i\t" >> statistics.txt
  # calculate average base pair length of the DMR
  awk '{ sum=$4-$3; if (sum > 0) print sum$6 }' cluster_"$i".txt  | awk '{ sum += $1; n++ } END { if (n > 0) printf sum / n"\t"; }' >> statistics.txt
  cut -f5 cluster_"$i".txt | sort | uniq -c | awk '{ printf $1"\t"}' >> statistics.txt
  printf "\n" >> statistics.txt
  rm cluster_*
done
