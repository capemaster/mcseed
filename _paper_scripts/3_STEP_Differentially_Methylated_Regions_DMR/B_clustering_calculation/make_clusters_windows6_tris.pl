#!/usr/bin/perl
use strict;
use lib "/bin";
use Getopt::Long;

my ($file_names, $out, $dist, $mode, $threshold, $warn_t, $prob);
GetOptions (
			"file_names=s" => \$file_names,"stringa_output=s"=>\$out, "max_dist=i"=>\$dist, "mode=s"=>\$mode, "threshold=s"=>\$threshold, "warn=i"=>\$warn_t, "prob=s"=>\$prob ) or die ("Error in command line arguments\n");
	

my @AoA;
#creo la lista dei file e dei campi#
my @array=split("£|-", $file_names);
my $count=0; my %hash_chr;
#carico tutti i file e aggiungo il valore dei campi inizio e fine 

my $ext_3;my $ext_5;
for (my $i=0; $i<scalar@array; $i=$i+6){
	my @file=&load_file_as_array("$array[$i]");
	$count++;
	for(my $j=0; $j<scalar@file; $j++) {
		chomp $file[$j];
		
		my @tmp=split ("\t", $file[$j]);
		push (@tmp, $array[$i]);
		$ext_5=$tmp[$array[$i+1]]+$array[$i+4];
		push (@tmp, $ext_5);
		$ext_3=$tmp[$array[$i+2]]+ $array[$i+5];
		push (@tmp, $ext_3);
		
		push (@{$hash_chr{$tmp[$array[$i+3]]}},[@tmp]) if $tmp[29] < $threshold;
							}
		
									}
								
							
my @keys_chr=keys(%hash_chr);

for (my $l=0; $l<scalar@keys_chr; $l++) {

my @AoA=@{$hash_chr{$keys_chr[$l]}};
#Faccio il sorting dei files
@AoA=sort{$a->[-2]<=>$b->[-2] || $b->[-1]<=>$a->[-1]}@AoA;

#Costruisco i clusters


my @AoA_cluster=();
my %hash;
my $countC=0;
my @primo;push (@primo, [@{$AoA[0]}]);
push (@AoA_cluster, [@primo]);
my $inizio=$AoA[0][-2]; 
my $fine=$AoA[0][-1]; 
$hash{$countC}{$AoA[0][-3]}++;
my @sep=("#######################");


for (my $i=1; $i<scalar@AoA; $i++){#
	
	my $verso=&determine_direction(\@{$AoA_cluster[-1][-1]},"31");
	my $verso_e=&determine_direction(\@{$AoA[$i]}, "31");
	
	if ((abs($AoA_cluster[-1][-1][-2]-$AoA[$i][-2])<=$dist)) {
		if (($verso_e eq $verso) && ($mode eq 'strict')){
			push(@{$AoA_cluster[-1]}, [@{$AoA[$i]}]);
			$hash{$countC}{$AoA[$i][-3]}++;
		}
		elsif ($mode  ne 'strict'){
			push(@{$AoA_cluster[-1]}, [@{$AoA[$i]}]);
			$hash{$countC}{$AoA[$i][-3]}++;
								}
		}
		

		else {
			push(@{$AoA_cluster[-1]}, [@sep]);
			$countC++;
			$hash{$countC}{$AoA[$i][-3]}++;
			my @new;
			push (@new, [@{$AoA[$i]}]);
			push(@AoA_cluster, [@new]);
			}
		}

&print_array(\@AoA_cluster, "bingo1", "APPEND");
my @output=split("£|-", $out);

my @keys=sort{$a<=>$b}(keys(%hash));

for (my $k=0; $k<scalar@keys; $k++){
	my @comp;
	my $SCORE=1;
	for (my $o=0; $o<scalar@output; $o=$o+2){
	my $score=0;
	if ((exists $hash{$keys[$k]}{$output[$o]}) && ($hash{$keys[$k]}{$output[$o]}>=$output[$o+1])) {$score=1;push (@comp,$hash{$keys[$k]}{$output[$o]}); }	
	$SCORE=$SCORE*$score;} 
	
	if ($SCORE>0){
		my $stampo_comp=join("\t", @comp);
		my $warn=&check_warn(\@{$AoA_cluster[$keys[$k]]}, "$prob");
		&print_array(\@{$AoA_cluster[$keys[$k]]}, "bingo", "APPEND") if $warn<=$warn_t;
		&print_array(\@{$AoA_cluster[$keys[$k]]}, "bingo_w", "APPEND") if $warn>$warn_t;
		print $AoA_cluster[$keys[$k]][0][0]."-".$AoA_cluster[$keys[$k]][-2][0]."\t".$AoA_cluster[$keys[$k]][0][1]."\t".$AoA_cluster[$keys[$k]][0][2]."\t".$AoA_cluster[$keys[$k]][-2][3]."\t$stampo_comp\n";
}
					}
	
undef @AoA_cluster;	
}

sub print_array {
	my @self=@_;
	my @AoA=@{$self[0]};
	my $nome_file=$self[1];
	my $mode_self=$self[2] || "OVERWRITE";
	
my $mode= '>';
$mode='>>'	if $mode_self eq "APPEND";

open (OUTARRAY, "$mode", "$nome_file") if $mode_self =~/APPEND|OVERWRITE/;
&p_array(\@AoA);
sub p_array {
    my $array_reference = shift;
	my $flag=0;
 
    foreach (@$array_reference) {
        if (ref($_) eq 'ARRAY') {
            &p_array($_);
        } else {
            print OUTARRAY $_ ."\t"  ;
            $flag=1;
        }
    
   }
print OUTARRAY "\n" if $flag==1;
}

close OUTARRAY;
}

sub load_file_as_array {
		my @self=@_;
		my $name_file=$self[0];
open(FILE, '<', "$name_file");
		my @array=(<FILE>);
close FILE;

		return @array;
					}
					
					
sub determine_direction {
	my @self=@_;
	my @array=@{$self[0]};
	my $field=$self[1]-1;
	my $value=$array[$field];
	my $verso=0;
	$verso=1 if $value >0;
	return $verso;
						}
	
sub check_warn {
	my @self=@_;
	my @array=@{$self[0]};
	my $prob=$self[1];
	my $count=0;
	for (my $i=0; $i<scalar@array; $i++){
		
		$count++ if $array[$i][23]>$prob;
	}
	return $count;
}
