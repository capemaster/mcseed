# WARNING: this is an example taken from our paper
# We choose MethylKit, but with the bismark format any other package could be used.

library(methylKit)

# these are in the rpm/ folder
file1<-"methylkit_format_stress1.txt"
file2<-"methylkit_format_stress2.txt"
file3<-"methylkit_format_stress3.txt"
file4<-"methylkit_format_wt1.txt"
file5<-"methylkit_format_wt2.txt"
file6<-"methylkit_format_wt3.txt"
file.list<-list(file1,file2,file3,file4,file5,file6)
file.list

## STOP_change context in the script
myobj<- read(file.list, sample.id=list("stress1","stress2","stress3","wt1","wt2","wt3"), assembly= "maize", treatment=c(1,1,1,0,0,0), context="CHG")

filtered.myobj<-filterByCoverage(myobj, lo.count=10, lo.perc=NULL, hi.count=NULL, hi.perc=99.9)

meth<- unite(filtered.myobj, destrand=FALSE)
write.table(meth, file="DMPs_meth.txt", sep="\t", row.names=TRUE, col.names=TRUE)

myDiff <- calculateDiffMeth(meth, mc.cores=4, adjust = "BH")
write.table(myDiff, file="DMPs_myDiff_BH.txt", sep="\t", row.names=TRUE, col.names=TRUE)

myDiff5p<-getMethylDiff(myDiff, difference=5, qvalue=0.05)
write.table(myDiff5p, file="DMPs_myDiff_diff5_BH005.txt", sep="\t", row.names=TRUE, col.names=TRUE)

myDiff5p.hyper<-getMethylDiff(myDiff, difference=5, qvalue=0.05, type="hyper")
write.table(myDiff5p.hyper, file="DMPs_myDiff_BH005_hyper.txt", sep="\t", row.names=TRUE, col.names=TRUE)

myDiff5p.hypo<-getMethylDiff(myDiff, difference=5, qvalue=0.05, type="hypo")
write.table(myDiff5p.hypo, file="DMPs_myDiff_BH005_hypo.txt", sep="\t", row.names=TRUE, col.names=TRUE)
