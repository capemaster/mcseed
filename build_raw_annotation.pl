# This Perl script is part of the ddRAD pipeline and is not intended to be
# launched alone. It depends on Bio::Cigar that can be easily installed using
# cpan with the following line

# cpan[1]> install Bio::Cigar

# Use force if some test fail.

# Please note that this scripts requires chromosomes named by number (1,2,3...)
use strict;
use 5.014;
use Bio::Cigar;

while (<>) {    # read the file line by line
    my @temp = split( /\t/, $_ );

# Skip Plastid, Mitochondrion and scaffolds (change if you have different naming)
    next
        if ( $temp[2] eq "Pt" || $temp[2] eq "Mt" || $temp[2] =~ "scaffold" );

    # Retrieve CIGAR field and calculate reference length
    my $cigar  = Bio::Cigar->new( $temp[5] );
    my $length = $cigar->reference_length;

  # Adjust read length according to 0 based coords and extrapolate START and END
  # This is necessary to avoid discrepancies in IGV for example
    my $start = $temp[3] - 1;
    my $end   = $temp[3] + $length - 1;

    if ( $temp[1] == "16" ) {    # if the reads aligns in the reverse strand
        printf "$temp[2]\t$start\t$end\t$temp[2]\:$start\-$end\t0\t-\n";
    }
    elsif ( $temp[1] == "0" ) {    # if the reads aligns in the forward strand
        printf "$temp[2]\t$start\t$end\t$temp[2]\:$start\-$end\t0\t+\n";
    }
}
