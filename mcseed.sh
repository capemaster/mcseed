#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                      __  __  _____  _____      ______    _                  #
#                     |  \/  |/ ____|/ ____|    |  ____|  | |                 #
#                     | \  / | |    | (___   ___| |__   __| |                 #
#                     | |\/| | |     \___ \ / _ \  __| / _` |                 #
#                     | |  | | |____ ____) |  __/ |___| (_| |                 #
#                     |_|  |_|\_____|_____/ \___|______\__,_|                 #
#                                                                             #
#                    Methylation Context Sensitive Enzyme ddRAD               #
#                           Ver 0.1.1 - APRIL  2021                           #
#                                                                             #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Indexing of the genome: this line is necessary the first time the pipeline is
# launched.
# Change the directory as needed and run the following line
# bwa index your.favourite.reference.assembly.fasta;


# DEPENDENCIES
# MCSeEd depends on several packages that have to be installed prior running
# the suite:
# Dependencies are listed below:

#  * BWA            [http://www.bio-bwa.sourceforge.net/]
#  * Bio::Cigar     [http://search.cpan.org/~tsibley/Bio-Cigar-1.01/lib/Bio/Cigar.pm]
#  * bedtools       [http://www.bedtools.readthedocs.io/]
#  * Stacks         [http://catchenlab.life.illinois.edu/stacks/]
#  * GNU Parallel   [http://www.gnu.org/software/parallel/]
#  * Rainbow        [http://www.sourceforge.net/projects/bio-rainbow/]
#  * samtools       [http://www.htslib.org/]
#  * cd-hit         [http://cd-hit.org/]
#  * featureCounts  [http://www.subread.sourceforge.net/]

# Anyway, MCSeEd will check and warn you if something is not in your $PATH.

# The directory structure should be prepared as follows:
# .
# ├── Name_Of_Your_Analysis	<-  Here you will place the scripts.
#     └── input_sequences	  <-  Here the cleaned sequences in fastq.
#
# Fastq files are intended to be gzipped. The extension should be fq.gz.

# Once the directory structure is created, and sequences are in place, just run:
# $ sh mcseed.sh
# or simply
# $ ./mcseed.sh

# The first input from the user is the choice between genome dependent od independent
# analysis.

# WARNING
# If you choose genome dependent, a genome in fasta has to be indicated. User
# is provided with shell autocompletion for convenience.

# If the user choose to perform the variant calling procedure, another directory
# named variantcalling/ will be created where working files will be stored.

# The second choice is the location of the genome (for genome dependent analysis)
# or the maximun number of threads that MCSeEd is allowed to use.

# OUTPUT
# MCSeEd results are stored in a directory named results_DATE, where DATE is the
# time stamp coherent with the log file creation.

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                               WELCOME MESSAGE                               #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Error checking by BASH
set -u
set -e


printf "Welcome to \n"
printf "     __  __  _____  _____      ______    _   \n "
printf "   |  \/  |/ ____|/ ____|    |  ____|  | |  \n "
printf "   | \  / | |    | (___   ___| |__   __| |  \n "
printf "   | |\/| | |     \___ \ / _ \  __| / _  |  \n "
printf "   | |  | | |____ ____) |  __/ |___| (_| |  \n "
printf "   |_|  |_|\_____|_____/ \___|______\__ _|  \n\n "
printf " Methylation Context Sensitive Enzyme ddRAD  \n\n"
printf "     ver 0.1.1 - last revision: 2021/04/29         \n\n\n"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                   READ THE PARAMETERS AND SETTING DEFAULTS                  #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Set the time for logging name purposes
log_prefix=`date +%Y.%m.%d.%H.%M`
path=`pwd`

# Small function to provide better logging
log() {
  msg=$1
  date=`date +%Y/%m/%d\|%H:%M`
  printf "$date - $msg\n" | tee -a $path/$log_prefix.log
}

# Checking if program dependencies are satisfied
command -v cd-hit >/dev/null 2>&1 || { log >&2 "There is no cd-hit in your path. Aborting.\n"; exit 1; }
command -v bedtools >/dev/null 2>&1 || { log >&2 "There is no bedtools in your path. Aborting.\n"; exit 1; }
command -v bwa >/dev/null 2>&1 || { log >&2 "There is no bwa in your path. Aborting.\n"; exit 1; }
command -v parallel >/dev/null 2>&1 || { log >&2 "There is no parallel in your path. Aborting.\n"; exit 1; }
perl -MBio::Cigar -e 0 > /dev/null 2>&1 || { log >&2 "There is no Bio::Cigar module installed. Aborting.\n"; exit 1; }
command -v rainbow >/dev/null 2>&1 || { log >&2 "There is no rainbow in your path. Aborting.\n"; exit 1; }
command -v sstacks >/dev/null 2>&1 || { log >&2 "There is no sstacks in your path. Aborting.\n"; exit 1; }
command -v samtools >/dev/null 2>&1 || { log >&2 "There is no samtools in your path. Aborting.\n"; exit 1; }
command -v featureCounts >/dev/null 2>&1 || { log >&2 "There is no featureCounts in your path. Aborting.\n"; exit 1; }

# Checking if program scripts are in the same directory
# build_raw_annotation.pl
if [ ! -f "build_raw_annotation.pl" ]
then
  log "There is no build_raw_annotation.pl file!"
  printf "Check the spelling or copy it the working directory\n\n"
  exit 1;
fi
# merge_overlapping_for_rev.pl
if [ ! -f "merge_overlapping_for_rev.pl" ]
then
  log "There is no merge_overlapping_for_rev.pl file!"
  printf "Check the spelling or copy it the working directory\n\n"
  exit 1;
fi
# Checking if input_sequences directory exists
if [ ! -d "input_sequences" ]
then
  log "There is no input_sequences/ directory in the working path!"
  printf "Check the spelling or copy it the working directory\n\n"
  exit 1;
fi
# Check if any fq.gz exists in the input_sequences directory.
ls input_sequences/*.fq.gz >/dev/null 2>&1 || { log >&2 "There is no *fq.gz file in the input_sequences/ directory!\n"; exit 1; }


# Set defaults for user choices
default_calling="no"
default_ref_genome="no"

# User input for reference genome
read 	-p "Is a reference genome available? (yes/no) [default: $default_ref_genome]: " ref_genome
ref_genome=${ref_genome:-$default_ref_genome}


# Creating Results directory
mkdir results_$log_prefix


if [ $ref_genome = "no" ];
then
  log "GENOME INDEPENDENT PIPELINE SETTINGS"

  # User input
  read 	-p "Perform variant calling? (yes/no) [default: $default_calling]: " calling
  read 	-p "Enter the maximum number of cores you want use: " ncores

  calling=${calling:-$default_calling}

  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  #                         GENOME INDEPENDENT ANALYSIS                         #
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

  log "Starting Genome Indepentent Pipeline"
  # Creating the directory structure
  mkdir -p mapped_samples
  mkdir -p counts
  # Creating the reference genome from individual reads

  cd input_sequences/
  # Merge all the sequences in one file
  log "Collapsing sequences"
  zcat ../input_sequences/*.fq.gz > merged.fastq
  log "Started assembly (takes time)"
  rainbow cluster -1 merged.fastq -m 2 > rainbow.cluster.out 2> rainbow.cluster.log;
  rainbow div -i rainbow.cluster.out -o rainbow.div.out
  rainbow merge -o rainbow.merge.out -a -i rainbow.div.out -N 500
  log "Refining assembly"
  select_best_rbcontig.pl rainbow.merge.out rainbow.div.out > rainbow.assembly.out
  cd-hit-est -i rainbow.assembly.out -o reference.assembly.fasta -mask N -M 20000 -T $ncores -c 0.95 1> /dev/null

  mv reference.assembly.fasta ../mapped_samples/

  cd ../mapped_samples
  # indexing the contigs
  log "Indexing the contigs"
  bwa index reference.assembly.fasta 2> /dev/null

  # Mapping on contigs
  log "Mapping on contigs"
  for i in $(ls ../input_sequences/*.fq.gz | xargs -n 1 basename); do
    log "Mapping $i"
    bwa mem -t $ncores -r 1 -c 1 -M reference.assembly.fasta ../input_sequences/$i > $i.sam 2> /dev/null
  done

  log "Processing BAMs and selecting uniquely mapped reads"
  ls *.sam | parallel 'samtools view -h -q 1 -F 4 -F 256 {} | grep -v "XA:Z" | grep -v "SA:Z" | samtools view -Sb | samtools sort - -o {.}.bam'

  ls *.bam | parallel 'samtools index {}'

  log "Counting reads on contigs"

  ls *.bam | parallel 'samtools idxstats {} > ../counts/{.}.counts.txt'

  ls *.bam | parallel 'samtools flagstat {} > {.}.stats.txt'

  cd ../counts

  ls *.txt | parallel 'cut -f1,3 {} > {.}.trim.txt'
  printf "ID" > counts_genome_independent.txt
  ls *counts.trim.txt | awk '{ printf "\t%s", $1 }'>>  counts_genome_independent.txt
  printf "\n" >> counts_genome_independent.txt
  paste *trim* | awk '{ for (i=3;i<=NF;i+=2) $i="" } 1' | tr -s ' ' '\t' >> counts_genome_independent.txt

  mv counts_genome_independent.txt  ../results_$log_prefix/

  log "Analysis finished, results available in results_$log_prefix/"


  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  #                      VARIANT CALLING GENOME INDEPENDENT                     #
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

  if [ $calling = "no" ];
  then
    log "No variant calling required, exiting."
  else
    log "Starting variant calling procedure..."

    # Resetting path
    cd ..

    # creates a directory for the working files
    mkdir variantcalling

    # initialize counter to identify samples
    counter=1

    for sample in $( ls input_sequences/*.fq.gz | grep -v merged ); do
      echo "processing sample ${sample} with ID ${counter}"
      # call loci with respect to reference
      ustacks -f ${sample} -i ${counter} -o variantcalling -p $ncores
      counter=$((counter+1))
    done

    # creating the population file that we need with populations
    cd input_sequences
    ls *.fq.gz | grep -v merged | sed 's/.fq.gz//' | awk '{print $1"\t"1}' > ../popfile.txt
    cd ..

    # recall samples path as prefix
    cstacks_line=`awk '{ printf "-s variantcalling/%s ", $1 }' popfile.txt`

    # assemble the catalog
    # Implement -n (number of mismatch)
    cstacks $cstacks_line -o ./variantcalling -p $ncores

    # match the sample snps to the catalog
    for sample in $( cut -f1 popfile.txt );
    do
      sstacks -c variantcalling/ -s variantcalling/$sample -g -o variantcalling -p 2
    done

    #  tsv2bam -P ./variantcalling/ -M popfile.txt  -t 8
    tsv2bam -P variantcalling/ -M popfile.txt  -t $ncores

    #  tsv2bam -P ./variantcalling/ -M popfile.txt  -t 8
    gstacks -P variantcalling/ -M popfile.txt  -t $ncores

    # create PLINK, STRUCTURE, GENEPOP and VCF files
    populations --in-path variantcalling/ --popmap popfile.txt --threads $ncores --vcf --genepop --structure --plink

    mv variantcalling/populations* results_$log_prefix/

    log "GENOME INDEPENDENT PIPELINE ENDED\nAll results should be available in the appropriate directories"
  fi

else

  log "GENOME DEPENDENT PIPELINE SETTINGS"

  read -e -p "Enter the full path where the script finds the genome in fasta: " genome_path
  # Check if the genome exists
  if [ ! -f "$genome_path" ]
  then
    log "There is no genome named $genome_path!"
    printf "Please insert the correct path\n"
    exit 1;
  fi

  read 	-p "Enter the maximum number of cores you want use: " ncores
  read 	-p "Perform variant calling? (yes/no) [default: $default_calling]: " calling

  calling=${calling:-$default_calling}

  log "MCSeEd GENOME DEPENDENT pipeline started"
  echo "Number of cores:	$ncores" >> $path/$log_prefix.log
  echo "Genome Path:		$genome_path" >> $path/$log_prefix.log

  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  #                         GENOME DEPENDENT ANALYSIS             			        #
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

  # Change directory and start mapping with bwa
  mkdir -p mapped_samples
  cd input_sequences

  for i in $(ls *fq.gz); do
    log "Mapping of $i initiated"
    bwa mem -t $ncores -r 1 -a -M $genome_path $i > ../mapped_samples/$i.sam;
    log "Mapping of $i finished"
  done
  log "Mapping Complete!"

  # filter uniquely mapped reads
  cd ../mapped_samples
  log "Uniquely mapped reads selection"
  ls *.sam | parallel 'samtools view -h -q 1 -F 4 -F 256 {} | grep -v "XA:Z" | grep -v "SA:Z" | samtools view -Sb > unique_{}.bam'
  # sort uniquely mapped reads
  log "Sorting and indexing .bam files"
  ls unique_* | parallel 'samtools sort {} -o sorted_{}'
  # Creating index
  # ls sorted_* | parallel 'samtools index {}'

  # Remove .sam  and temporary .bam files to save space
  rm -rf *.sam
  rm -rf unique_*.bam

  # Merging all the bamfiles in one and index it
  log "Merging and indexing .bam files"
  samtools merge -1 --threads $ncores merged.bam *.bam
  # samtools index merged.bam

  # The idea here is to create an annotation with all the genomic positions and
  # use this annotation to map the uniquely mapped reads.
  # Scaffolds, Pt and Mt mapping reads are removed, and coordinates are adjusted.
  # For advanced users, the perl script is commented.
  # Briefly, for each alignment in the bam file, genome coordinates and CIGAR
  # field are processed to produce intervals (perl). Then, identical lines are
  # collapsed and sorted.
  log "Creating annotation - (long step, single core)"
  samtools view merged.bam | perl ../build_raw_annotation.pl | uniq | sort -k1,1 -k2,2g > annotation_raw.bed

  # Merge overlapping sites preserving strand and counting how many reads
  # contribute to the feature (score field). Naming is provided in the fifth
  # column as comma separated list of the collapsed locations.
  log "Collapsing annotation"
  bedtools merge -s -c 4,4,6 -o collapse,count,distinct -i annotation_raw.bed > annotation_merged.bed

  # Create a fake GFF for counting purposes adding +1 to start as required by GFF specs
  log "Creating GFF"
  awk '{print $1"\tbed2gff\texon\t"$2+1"\t"$3"\t"$5"\t"$6"\t.\tgene_id \""$1":"$2+1"-"$3"\"; site_id \""$1":"$2+1"-"$3"\";"}' annotation_merged.bed > annotation_merged.gff

  # Prepare bamfiles list
  bamfiles=`ls sorted*.bam  | awk -vORS=" " '{ print $1 }' | sed 's/ $//'`

  # count all the reads in the bam files
  # -T $ncores	number of threads set as decided at the beginning
  # -Q 1				minimum quality of the alignment is set to 1 (increase this if you need)
  # -s 1				account for strandness
  # Check featureCounts manual to enhance your count matrix
  log "Counting reads on annotation"
  featureCounts -T $ncores -Q 1 -s 1 -a annotation_merged.gff -o featurecounts_results_matrix.txt $bamfiles

  # Creating non overlapping +/- fragments and saving the final count matrix
  log "Creating final output"
  # Creating header
  sed '2q;d' featurecounts_results_matrix.txt > counts_genome_dependent.txt
  # Removing ambiguous lines +;- -;+ from the count matrix
  tail -n +3 featurecounts_results_matrix.txt | grep -v ";" > tmp
  perl ../merge_overlapping_for_rev.pl tmp >> counts_genome_dependent.txt
  rm tmp

  # moving results to correct directory
  mv counts_genome_dependent.txt  ../results_$log_prefix/

  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  #                      VARIANT CALLING GENOME DEPENDENT                       #
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

  if [ $calling = "no" ];
  then
    log "No variant calling required."
  else
    log "Starting variant calling procedure..."

    # Resetting path
    cd ..

    # Gstacks pipeline
    # creates a directory for the working files and temporary folder for bamfiles
    mkdir variantcalling
    mkdir bamforgstacks
    cp mapped_samples/sorted_unique*bam bamforgstacks

    # creating the population file that we need with gstacks and populations
    cd bamforgstacks
    ls *.bam | grep -v merged | sed 's/.bam//' | awk '{print $1"\t"1}' > ../popfile.txt
    cd ..

    # Running Gstacks
    gstacks -I bamforgstacks  -O variantcalling -M popfile.txt -t $ncores

    # create PLINK, STRUCTURE, GENEPOP and VCF files
    populations --in-path variantcalling/ --popmap popfile.txt --threads $ncores --vcf --genepop --structure --plink
    mv variantcalling/populations* results_$log_prefix/

    # Save space on disk
    rm -rf bamforgstacks

    log "GENOME DEPENDENT PIPELINE ENDED\nAll results should be available in the appropriate directories"
  fi
  # end of genome dependency choice
  # moving results into directories
  # mv mapped_samples/counts_*dependent.txt results_$log_prefix.txt
fi
