## What is MCSeEd?

**`MCSeEd`** (Methylation Content Sensitive Enzyme ddRAD) is a very simple, highly scalable, cost-effective extension of the original ddRAD protocol that allow the detection of methylation changes for the CG, CHG, CHH, and 6mA contexts.
In this repository the user will find the secessary instruction to analyze data obtained with the "wet lab" technique.

## Overview

MCSeEd pipeline is composed by three main programs:

> -   `mcseed.sh`	- The bash wrapper. This is the program you want to run.
> -   `build_raw_annotation.pl`	-	Script invoked inside the bash wrapper that deals with the annotation.
> -   `merge_overlapping_for_rev.pl` -Script invoked inside the bash wrapper that merges and cleans the raw annotation.

and a series of ancillary scripts and demo datasets contained in the directory `_paper_scripts`, that can be used to replicate the DMP and the DMR analysis as done in our paper.
**Please note** that this folder contain our working scripts an that **they would need some customization** basing on your unique experimental design. Explanations concerning flags and choices are in commented in the files. Anyway, we would be more than happy to help if needed.


## Usage

### Installation

Installing **`MCSeEd`** is an easy task: simply [download](https://bitbucket.org/capemaster/mcseed/downloads) the repository in zip format  or clone the repository in any location in your machine. The command to clone the repository in your system is the following:

```
$ git clone --recursive https://bitbucket.org/capemaster/mcseed.git
```

Once the **`MCSeEd`** directory is available, the program should be ready to run, if dependencies are satisfied.

### Dependencies

**`MCSeEd`** relies on several dependencies that can be easily fulfilled.
If the user has some experience with NGS data, most of them could be already installed.

Dependencies and URLs to donwload and install is provided:

-   BWA 0.7.15            [http://www.bio-bwa.sourceforge.net/]
-   bedtools 2.27      [http://www.bedtools.readthedocs.io/]
-   Stacks 2.3         [http://catchenlab.life.illinois.edu/stacks/]
-   GNU Parallel 20180322   [http://www.gnu.org/software/parallel/]
-   Rainbow 2.0.4       [http://www.sourceforge.net/projects/bio-rainbow/]
-   samtools 1.9      [http://www.htslib.org/]
-   cd-hit 4.8.1        [http://cd-hit.org/]
-   featureCounts 1.6 [http://www.subread.sourceforge.net/]
-   Bio::Cigar     [http://search.cpan.org/~tsibley/Bio-Cigar-1.01/lib/Bio/Cigar.pm]

Programs and languages are required to be in your **`$PATH`** environment variable.
If you don't know how to do this, refer to [this page](http://unix.stackexchange.com/questions/26047/how-to-correctly-add-a-path-to-path). Please note that the correct `$PATH` setting procedure is shell dependent. To check which shell environment you are running type this command:

`$ ps -p $$`
or

`$ echo $0`

Some dependencies can be satisfied also using dedicated package managers. For example [HomeBrew](http://www.brew.SH/) on macOS, or [yum](http://yum.baseurl.org/) on several Linux installations. However, there could be discrepancies between the availability of the lastest version from the developers and the one provided with the packet manager (usually older).

Please note that **`MCSeEd`** also depends on other bash utilities that are commonly installed on Linux/Unix/Mac systems. You should not have problems after dependencies satisfaction, but please report any problem or bug at [this page](https://bitbucket.org/capemaster/mcseed/issues/new).

**`MCSeEd`** will check and warn you if something is not in your `$PATH`.

### Running MCSeEd

**`MCSeEd`** requires a specific but simple directory structure:

    ├── Name_Of_Your_Analysis	<-  Here you will place the pipeline scripts.
        └── input_sequences	  <-  Here the cleaned (with **process_radtags**) sequences in fastq.

**Fastq files are intended to be gzipped**. The extension should be **fq.gz**.

Once the directory structure is created, and sequences are in place, just run **`$ sh mcseed.sh`** or just **`$ ./mcseed.sh`**.
If you receive this error

**`$ bash: ./mcseed.sh: Permission denied`**

try

**`$ chmod +x mcseed.sh`**


to make the file executable and re-run the command.


**`MCSeEd`** will immediately check for dependencies and log out if something is not present in your systam as expected. If everything is OK, the program will ask ti choose between **genome dependent** or **genome independent** analysis.

#### WARNING
If you choose genome dependent, a genome in fasta has to be indicated.
User is provided with shell autocompletion to ease the correct path insertion.

The program automatically skips *Mt*, *Pt* and *scaffold* named sequences. User can customize pattern matching in `build_raw_annotation.pl`.

The second question is the **location of the genome** (for genome dependent analysis) and/or the maximun **number of threads** that **`MCSeEd`** is allowed to use.

**`MCSeEd`** then asks if a **variant calling procedure** is required.
If this is the case, another directory `variantcalling/` will be created where working files will be stored.

## OUTPUT

**`MCSeEd`** results are stored in a directory named `results_DATE`, where DATE is the time stamp coherent with the log file creation.

MCSeEd has been successfully tested on the following systems:

-   Mac OSX 10.x and above
-   CentOS 7.x
-   Fedora 29

There should be no reason preventing any \*NIX syste, once dependencies are satisfied, from running  **`MCSeEd`**.

### Performance

Depending on your system, sequencing depth per sample and the level of parallelization you choose, a complete MCSeEd analysis can span from a few minutes to hours.
MCSeEd performance on a distributed analysis (20 CPUs, Intel Xeon X5675 @ 3.07 GHz) are showed below.

|           |  1 million reads per sample | 10 million reads per sample |
| --------- | :-------------------------: | :-------------------------: |
| 3 samples | 8 minutes / 2.5 Gb RAM Peak |  20 minutes / 8 Gb RAM Peak |

### Checking the results

If the analysis ends correctly, all your results should be contained in the `results_DATE/` directory.

The results consists in a count matrix, tab separated as described below.
ID (locus) is constructed  to friendly inspect the locus on the favourute genome browser (ie IGV).

| ID            |  Chr |  Start |    End | Strand | Length | S1 | S2 | S3 | C1 | C2 | C3 |
| ------------- | ---- | ------ | ------ | :----: | ------ | -- | -- | -- | -- | -- | -- |
| 1:15644-15766 | True | 15.644 | 15.766 | +      |    123 |  0 |  0 |  3 |  0 |  0 |  0 |
| 1:17645-17871 | True | 17.645 | 17.871 | -      |    227 |  0 |  1 |  0 |  1 |  0 |  0 |
| 1:17981-18051 | True | 17.981 | 18.051 | -      |     71 |  0 |  0 |  0 |  1 |  1 |  0 |
| 1:20415-20518 | True | 20.415 | 20.518 | +      |    104 |  1 |  1 |  0 |  0 |  1 |  0 |
| 1:22293-22424 | True | 22.293 | 22.424 | -      |    132 |  0 |  2 |  0 |  0 |  0 |  2 |
| 1:22612-22754 | True | 22.612 | 22.754 | +      |    143 |  0 |  0 |  0 |  0 |  0 |  1 |
| 1:24225-24346 | True | 24.225 | 24.346 | +      |    122 |  0 |  1 |  0 |  1 |  2 |  0 |
| 1:27536-27739 | True | 27.536 | 27.739 | +      |    204 |  2 |  1 |  1 |  1 |  1 |  0 |
| 1:27869-28047 | True | 27.869 | 28.047 | -      |    179 |  1 |  0 |  0 |  0 |  1 |  0 |

Scripts to normalize, filter and parse as in our paper are available upon request. (Folr)

Variant calling results consist in VCF catalogs and various input files for population genetics analysis (ie PED/MAP, Structure etc..)


### Windows users

Windows users cannot use **`MCSeEd`** out of the box. We suggest to install a Linux Virtual Machine using the free utility [VirtualBox](http://www.virtualbox.org).

## Citation

If you use **`MCSeEd`**, please cite our paper published in Scientific Reports along with the other tools used in the suite.

**Methylation content sensitive enzyme ddRAD (MCSeEd): a reference-free, whole genome profiling system to address cytosine/adenine methylation changes**
Marconi Gianpiero, Capomaccio Stefano, Comino Cinzia, Acquadro Alberto, Portis Ezio, Porceddu Andrea, Albertini Emidio. Scientific Reports volume 9, Article number: 14864 (2019) doi: [10.1038/s41598-019-51423-2](http://dx.doi.org/10.1038/s41598-019-51423-2)

## Disclaimer

**`MCSeEd`** is a free tool that uses free software that is publicly available online: you can redistribute this pipeline and/or modify this program, but at your own risk. **`MCSeEd`** is distributed with the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details: <http://www.gnu.org/licenses/>.
This pipeline is for research and has not a commercial intent, but it can be used freely by any organization. The authors of this pipeline are not responsible for ANY output, modification or result obtained from it. For bug report, feedback and questions (PLEASE read the carefully this README file before sending your question) please point your browser to [this page](https://bitbucket.org/capemaster/MCSeEd/issues/new).
